package bayern.kickner.logger;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class LoggerInitException extends Exception {

    public LoggerInitException() {
        super();
    }

    public LoggerInitException(String message) {
        super(message);
    }

    public LoggerInitException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoggerInitException(Throwable cause) {
        super(cause);
    }
}
