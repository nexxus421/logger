package bayern.kickner.logger;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Debug;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Class to use Androids logcat to write those logs to a logfile. Includes some configurations possibilities.
 * More logcat commands: <a href="https://developer.android.com/studio/command-line/logcat>Logcat Doc</a>
 * Needs write permission to external storage!
 * This Library is currently not supporting Android 10. You should compile with API 28.
 * <p>
 * Klasse um Androids Logcat in eine Datei schreiben zu lassen, inklusive Konfigurationsmöglichkeiten.
 * Weitere Kommandozeilenbefehle: <a href="https://developer.android.com/studio/command-line/logcat>Logcat Doc</a>
 * Benötigt Schreibrechte auf den externen Speicher.
 * Diese Library unterstützt derzeit nicht Android 10. Es sollte vorerst mit API 28 kompiliert werden.
 *
 * @author Marvin Kickner
 */
public class Logger {

    public static final String GLOBAL_TAG = "indiv.logger";
    private static final String TAG = Logger.class.getName();
    private static final Logger instance = new Logger();

    //Alle StringBuilder in #prepareStringBuilder einfügen
    private final StringBuilder logLevel;
    private final StringBuilder options;
    private final StringBuilder outputFormats;
    private final StringBuilder formatModifiers;
    private final StringBuilder logBuffer;
    private final StringBuilder rawOptions;
    private final StringBuilder rotator;
    @Nullable
    private String pathToLogFolder = null;
    @Nullable
    private String logFileName = null;
    private Process currentProcess = null;
    private boolean isCreated = false;

    private Logger() {
        logLevel = new StringBuilder(" ");
        options = new StringBuilder(" ");
        outputFormats = new StringBuilder(" ");
        formatModifiers = new StringBuilder(" ");
        logBuffer = new StringBuilder(" ");
        rawOptions = new StringBuilder(" ");
        rotator = new StringBuilder(" ");
    }

    public static @NonNull
    Logger getInstance() {
        return instance;
    }

    /**
     * Creates a TAG with a specific loglevel.
     * e.g. Every log with the TAG "Banana" should be Logged when the loglevel is higher than LogLevel.WARNING
     * Log.w("Banana", "Example"); --> Will be logged
     * Log.d("Banana", "Example"); --> Will not be logged
     *
     * @param tag      Tag that should be logged
     * @param logLevel Loglevel which should be watched
     * @return TAG-configuration as prepared String
     */
    public static @NonNull
    String getLogLevelForTag(@NonNull final String tag, @NonNull final LogLevel logLevel) {
        return tag + ":" + logLevel.getLogLevelShort();
    }

    /**
     * Resets all settings by clearing all StringBuilders
     *
     * @return this
     */
    public Logger resetSettings() {
        clearStringBuilder(logLevel);
        clearStringBuilder(options);
        clearStringBuilder(outputFormats);
        clearStringBuilder(formatModifiers);
        clearStringBuilder(logBuffer);
        clearStringBuilder(rawOptions);
        clearStringBuilder(rotator);
        return instance;
    }

    /**
     * Defines the watched loglevels for Logcat
     *
     * @param globalLogLevel Loglevel for the whole app.
     *                       Every Log that is not covered through {@param logLevelClass} will have to pass the globalLogLevel examination.
     * @param logLevelClass  Set loglevel for a single log/method/class/etc
     *                       Format --> [tag_name]:[LogLevel] - e. g. my.package.MainActivity:I or Banana:W
     *                       Use {@link Logger#getLogLevelForTag(String, LogLevel)} to build one of these.
     * @return this
     */
    public @NonNull
    Logger setLogLevel(@Nullable final LogLevel globalLogLevel,
                       @Nullable final String... logLevelClass) {
        if (logLevel.length() > 1) {
            Log.w(TAG, "Logger: Contains previous LogLevel settings. Should be cleared before setting new one.");
        }
        //Not catched exceptions are named AndroidRuntime
        //We always want to see those exceptions
        logLevel.append(" AndroidRuntime:V ");

        if (logLevelClass != null) {
            for (final String s : logLevelClass) {
                if (s.contains(":")) logLevel.append(" ").append(s).append(" ");
            }
        }

        //Setzt Loglevel für alle Logs, die nicht extra in #logLevelClass definiert wurden
        if (globalLogLevel != null)
            logLevel.append(" *:").append(globalLogLevel.getLogLevelShort());

        if (globalLogLevel == null && logLevelClass == null) {
            Log.w(TAG, "No LogLevel specified. Everything will be logged to file...");
        }
        return instance;
    }

    /**
     * Clears current loglevel settings and set new settings
     * see {@link #setLogLevel(LogLevel, String...)}
     *
     * @param globalLogLevel globalLogLevel
     * @param logLevelClass  logLevelClass
     * @return this
     */
    public @NonNull
    Logger clearAndSetLogLevel(@Nullable final LogLevel globalLogLevel,
                               @Nullable final String... logLevelClass) {
        clearStringBuilder(logLevel);
        return setLogLevel(globalLogLevel, logLevelClass);
    }

    /**
     * The rotator creates a new file for logging after the current filesize reached {@param kbyte}.
     * Then a new file will be created with the suffix from the current rotation-number.
     * The limit of rotation-logs will be defined through {@param numOfRotatedLogs} .
     * <p>
     * If every logfile reached the maximum filesize, the oldest logs will be deleted to make space for new logs.
     *
     * @param kbyte            maximum filesize for a single logfile
     * @param numOfRotatedLogs maximum count of rotate-filelogs
     * @return this
     */
    public @NonNull
    Logger setRotator(@IntRange(from = 1) final int kbyte,
                      @IntRange(from = 1) final int numOfRotatedLogs) {
        if (rotator.length() > 1) {
            Log.w(TAG, "Logger: Contains previous rotator settings. Should be cleared before setting new one.");
        }
        rotator.append("-n ").append(numOfRotatedLogs).append(" -r ").append(kbyte).append(" ");
        return instance;
    }

    /**
     * Deletes the current configuration and sets the new rotator settings
     * see {@link Logger#setRotator(int, int)}
     *
     * @param kbyte            kbyte
     * @param numOfRotatedLogs numOfRotatedLogs
     * @return this
     */
    public @NonNull
    Logger clearAndSetRotator(@IntRange(from = 1) final int kbyte,
                              @IntRange(from = 1) final int numOfRotatedLogs) {
        clearStringBuilder(rotator);
        return setRotator(kbyte, numOfRotatedLogs);
    }

    public @NonNull
    Logger setLogPath(@NonNull final String pathToLogFolder, @NonNull final String logFileName) {
        this.pathToLogFolder = pathToLogFolder;
        this.logFileName = logFileName;
        return instance;
    }

    /**
     * Read settings and runs logcat with this configuration.
     *
     * @param context this
     */
    public void init(@NonNull final Context context, final boolean ignoreWhenDebuggerConnected) throws LoggerInitException {

        if (ignoreWhenDebuggerConnected && Debug.isDebuggerConnected()) {
            Log.i(TAG, "File-logging will be ignored. Debugger connected. Logging to stout.");
            return;
        }

        if (isExternalStorageWriteable(context)) {

            if (currentProcess != null) currentProcess.destroy();

            final File logFile = createAndGetLogfile();
            if (logFile == null) {
                Log.e(TAG, "Error creating log-folder.");
                return;
            }
            try {
                final String config = createLogcatConfig();
                currentProcess = Runtime.getRuntime().exec("logcat -c"); //clean log
                currentProcess = Runtime.getRuntime().exec("logcat " + config + " -f " + logFile);

                System.out.println("Config: " + config);
                Log.i(TAG, "Logger: Logger created successfully");
                Log.i(TAG, "Logger: Config: " + config);
                Log.i(TAG, "Logger: Path: " + pathToLogFolder + ", LogFileName: " + logFileName);
                isCreated = true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "Logger#init", e);
                isCreated = false;
            }

        } else {
            Log.e(TAG, "No write permission.");
            isCreated = false;
            throw new LoggerInitException("No write permission for Logger");
        }
    }

    /**
     * Creates and prepare one configuration-String from all seperated settings-StringBuildes
     *
     * @return Configuration which will be inserted between "logcat" and "-f [...]".
     */
    private @NonNull
    String createLogcatConfig() {
        final String loggerTag = " " + TAG + ":I";
        if (!logLevel.toString().contains(loggerTag)) {
            logLevel.append(loggerTag);
        }
        final String configuration = logLevel.append(rotator)
                .append(options)
                .append(outputFormats)
                .append(formatModifiers)
                .append(logBuffer)
                .append(rawOptions)
                .toString();
        return configuration.trim().replaceAll(" +", " ");
    }

    /**
     * Clears a StringBuilder
     *
     * @param sb StringBuilder which will we cleared and set to length 0.
     */
    private void clearStringBuilder(@NonNull final StringBuilder sb) {
        sb.setLength(0);
        sb.append(" ");
    }

    /**
     * Checks if logfolder and logfile exists and create them if necessary.
     *
     * @return logfile or null for errors
     */
    private @Nullable
    File createAndGetLogfile() {
        if (pathToLogFolder == null)
            pathToLogFolder = Environment.getExternalStorageDirectory() + File.separator + "Log"; //Default Pfad, falls dieser nicht gesetzt wird
        if (logFileName == null)
            logFileName = "logfile.log"; //Default Name, falls dieser nicht gesetzt wird
        final File logDir = new File(pathToLogFolder);
        final File logFile = new File(logDir, logFileName);//"LOG_" + System.currentTimeMillis() + ".log");

        if (!logDir.exists()) {
            if (!logDir.mkdirs()) {
                //ToDo: Wie am besten Negativ-Fall behandeln?
                Log.e(TAG, "Log-folder could not be created! Logging not possible!");
                return null;
            }
        }
        return logFile;
    }

    /**
     * Checks for write-permission to external storage
     *
     * @return true if succes else false
     */
    private boolean isExternalStorageWriteable(@NonNull final Context context) {
        final String state = Environment.getExternalStorageState();
        boolean allowedWriteToExternalStorage = true;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            final int writePermission = context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            allowedWriteToExternalStorage = writePermission == PackageManager.PERMISSION_GRANTED;
        }
        return Environment.MEDIA_MOUNTED.equals(state) && allowedWriteToExternalStorage;
    }

    /**
     * This method provides the option to add settings which are not covered through other methods here.
     * Use at your own risk!
     * You should read the official logcat doc from Android.
     *
     * @param options settings for logcat.
     * @return this
     */
    public @NonNull
    Logger setRawOptions(@NonNull final String... options) {
        rawOptions.setLength(0);
        rawOptions.append(" ");
        for (String option : options) {
            rawOptions.append(option).append(" ");
        }
        return instance;
    }

    public boolean isCreated() {
        return isCreated;
    }

    public @NonNull
    String getPathToLogFile() {
        return pathToLogFolder + File.separator + logFileName;
    }

    /**
     * ToDo: Not yet implemented
     * Change the log-formatting. E. g. timestamp, PID, metadata.
     *
     * @return this
     */
    public @NonNull
    Logger setOutputFormat() {
        return instance;
    }

//	/**
//	 * Definieren verschiedener Einstellungen für Logcat.
//	 *
//	 * @return Aktuelle Instanz
//	 * @deprecated Vorerst nicht benötigt und daher nicht implementiert.
//	 */
//	public @NonNull
//	Logger setOptions() {
//		return instance;
//	}

//	/**
//	 * Kombination mit OutputFormats möglich. Setzt einzelne Formatierungen der Logs.
//	 *
//	 * @return Aktuelle Instanz
//	 * @deprecated Vorerst nicht benötigt und daher nicht implementiert.
//	 */
//	@Deprecated
//	public @NonNull
//	Logger setFormatModifiers() {
//		return instance;
//	}

//	/**
//	 * Einstellen der zu nutzenden Buffer. Jeder Buffer enthält unterschiedliche Informationen.
//	 *
//	 * @return Aktuelle Instanz
//	 * @deprecated Vorerst nicht benötigt und daher nicht implementiert.
//	 */
//	@Deprecated
//	public @NonNull
//	Logger setLogBuffer() {
//		return instance;
//	}

    @Nullable
    public String getPathToLogFolder() {
        return pathToLogFolder;
    }

    @Nullable
    public String getLogFileName() {
        return logFileName;
    }

    /**
     * Different types of available loglevels
     */
    public enum LogLevel {
        VERBOSE("V", "Verbose"),
        DEBUG("D", "Debug"),
        INFO("I", "Info"),
        WARNING("W", "Warn"),
        ERROR("E", "Error"),
        FATAL("F", "Fatal"),
        SILENT("S", "Silent");
        final String logLevelShort;
        final String logLevel;

        LogLevel(final String logLevelShort, final String logLevel) {
            this.logLevelShort = logLevelShort;
            this.logLevel = logLevel;
        }

        public String getLogLevel() {
            return logLevel;
        }

        public String getLogLevelShort() {
            return logLevelShort;
        }

        /**
         * Sucht passenden LogLevel anhand des {@link LogLevel#logLevel} oder {@link LogLevel#logLevelShort}.
         *
         * @param logLevel Gesuchtes LogLevel als String
         * @return LogLevel oder null falls nicht existent
         */
        public static @Nullable
        LogLevel getLogLevelFromString(@NonNull final String logLevel) {
            for (LogLevel entry : values()) {
                if (entry.getLogLevel().equalsIgnoreCase(logLevel) ||
                        entry.getLogLevelShort().equalsIgnoreCase(logLevel)) {
                    return entry;
                }
            }
            return null;
        }
    }
}